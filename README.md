# delf-calculator

## Env.
* Python 3.6.5
* pandas

## Run.
* コマンドライン引数を使っています。
* 基本形

```
python exe.py "[ファイル名]" "[アクセストークン]"

# ex: 末田くんの場合
python exe.py "001sueda" "[アクセストークン]"
```


## アクセストークン一覧
* test（※鶴岡開発用）
    * TGx4onXvSBKPUT8DodXOvB0LRnm078edn7tzFfBNIs0
* 001 末田くん
    * 4SCo98494UVxB13Z4o1DxN67DMJufp1A36wPkJft7NA
* 002 荒木くん
    * yXPj1jb8ZbTMqDvHHF50BoOHv3rh8bzfuYvjWW2f15V

* 003 徳丸くん
    * RtWPIrwk3JQD1lbtGzbkcaABKbvm1anjRqcmHiH51H1


## 運用
* 環境はcondaで管理（delfenv.yaml）

```
# 保存
$ conda env export -n [env_name] > env_name.yml

# 構築
$ conda env create -f=env_name.yml
```

## condaメモ
```
# env確認
$ conda info --envs

# アクティベイト
$ source activate myenv

# 環境から出る
$ source deactivate
```


## 環境構築手順
* pyenvをインストール
* minicondaをインストール
* yamlファイルからconda環境を作成
