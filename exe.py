# coding: UTF-8
import requests
import sys
import os
from datetime import datetime as dt

'''pip package'''
import pandas
import matplotlib.pyplot as plt
import seaborn as sns

GOOGLE_FORM_URL = 'google form URL'

OUTPUT_COLUMNS = ['timestamp', 'score', 'shot_count']
RAW_FILE = "raw.csv"
DAILY_FILE = "daily.csv"
IMG_FILE = 'img.png'
LATEST_FILE = 'latest_date.txt'


class Execute:
    def __init__(self, name):
        self.name = name
        self.output_path = "{}/data/students/{}".format(os.getcwd(), name)
        self.raw_path = "{}/{}".format(self.output_path, RAW_FILE)
        self.daily_path = "{}/{}".format(self.output_path, DAILY_FILE)
        self.img_path = "{}/{}".format(self.output_path, IMG_FILE)
        self.latest_path = "{}/{}".format(self.output_path, LATEST_FILE)
        self._mkdir_if_not()
        print(self.output_path)


    def _mkdir_if_not(self):
        os.makedirs(self.output_path, exist_ok=True)

    def make_msg(self, diff, score, shot_count, date):
        if diff > 5:  # +5以上（x > 5）のとき
            msg = "[{}] 今回は{}点（{}ショット）！前回よりもかなり集中できてていい感じ！ちなみに前回と変わったことはある？練習前の準備・食事・睡眠・学校生活‥‥ 思いついたこと教えて！({})".format(
                date, score, shot_count, GOOGLE_FORM_URL)
        elif diff < -5:  # -5未満( x < 5)のとき
            msg = "[{}] 今回は{}点（{}ショット）！いつもより点数は低いけど、納得感あるかな？ちなみに前回と変わったことはある？練習前の準備・食事・睡眠・学校生活‥‥ 思いついたこと教えて！({})".format(
                date, score, shot_count, GOOGLE_FORM_URL)
        else:  # -5〜+5(-5 <= x <= 5)のとき
            msg = "[{}] 今回は{}点（{}ショット）！前回の調子を維持できています。この調子でいこう！ちなみに前回と変わったことはある？練習前の準備・食事・睡眠・学校生活‥‥ 思いついたこと教えて！({})".format(
                date, score, shot_count, GOOGLE_FORM_URL)
        return msg

    def save_file(self, val):
        with open(self.latest_path, 'w') as f:
            f.write(str(val))


    def read_file(self):
        try:
            with open(self.latest_path) as f:
                s = f.read()
        except:
            return None
        return s




class Calculate:
    def __init__(self, filename):
        self.dataset = pandas.read_csv(filename)
        print('[count] raw data: {}'.format(self.dataset['timestamp'].count()))
        self._extract_useable_data()
        begin_date = self.dataset.head(1)['timestamp'].values[0]
        end_date = self.dataset.tail(1)['timestamp'].values[0]
        self.begin_day, new_time = begin_date.split()
        self.end_day, new_time = end_date.split()
        # for output.
        self.res_df = pandas.DataFrame(index=[], columns=OUTPUT_COLUMNS)
        print("{}〜{}".format(self.begin_day, self.end_day))

    def _extract_useable_data(self):
        ''' 不要なデータ（スイングではない）データを削除 '''
        self.dataset = self.dataset[self.dataset['swing_speed[km/h]'].notnull()]
        print('[count] extracted data: {}'.format(
            self.dataset['timestamp'].count()))

    def calc_score(self):
        ''' 各ショットのスコアを計算 '''
        for index, row in self.dataset.iterrows():
            ball_speed = row['ball_speed[km/h]']
            swing_speed = row['swing_speed[km/h]']
            ball_spin = row['ball_spin']
            self.dataset.at[index, 'score'] = int(
                ball_speed - swing_speed) + (ball_spin * 2)

    def export_daily_data(self):
        ''' 日別に計算 '''
        base_day = dt.strptime(self.begin_day, '%Y-%m-%d')
        count = 0
        score_sum = 0
        self.dataset = self.dataset.append(self.dataset.tail(1))
        self.dataset.tail(1)['timestamp'] =  '9999-01-01 00:00:00' #dummy.
        for index, row in self.dataset.iterrows():
            date, time = row['timestamp'].split(' ')
            day = dt.strptime(date, '%Y-%m-%d')
            diff = (day - base_day).days
            # print("day: {}, diff: {}".format(day, diff))
            if diff < 1:
                count += 1
                score_sum += row['score']
                continue

            print("{}: count {}, score_sum: {}".format(base_day, count, score_sum))
            res = int(score_sum / count)
            series = pandas.Series(
                [base_day.strftime("%Y%m%d"), res, count], index=self.res_df.columns)
            self.res_df = self.res_df.append(series, ignore_index=True)

            base_day = day
            count = 1
            score_sum = row['score']

    def extract_useable_daily_data(self, start_date, daily_path):
        self.new_dataset = pandas.read_csv(daily_path)
        idx_start = self.new_dataset.reset_index().query(
            'timestamp == {}'.format(start_date)).index[0]
        idx_end = self.new_dataset.tail(1).index[0]
        print('new_dataset data idx: {}〜{}'.format(idx_start, idx_end))
        self.new_dataset = self.new_dataset[idx_start:idx_end + 1]


class Notification:
    def __init__(self):
        self.line_notify_api = 'https://notify-api.line.me/api/notify'
        self.headers = {'Authorization': 'Bearer ' + line_notify_token}
        pass

    def post_simple(self, msg):
        payload = {'message': msg}
        line_notify = requests.post(
            self.line_notify_api, data=payload, headers=self.headers)
        print(line_notify.status_code)

    def post_with_img(self, msg, img_path):
        payload = {'message': msg}
        files = {"imageFile": open(img_path, "rb")}
        line_notify = requests.post(
            self.line_notify_api, data=payload, headers=self.headers, files=files)
        print(line_notify.status_code)



if __name__ == '__main__':
    args = sys.argv
    print(args)
    name = args[1]
    line_notify_token = args[2]
    exe = Execute(name=name)

    # 日付の整形データをcsv出力
    calc = Calculate(exe.raw_path)
    calc.calc_score()
    calc.export_daily_data()
    print(calc.res_df)
    calc.res_df.to_csv(exe.daily_path)

    # チャート出力
    sns.set()
    sns.set_context("paper", 1.0, {"lines.linewidth": 1.5})
    plt.plot(calc.res_df['timestamp'], calc.res_df['score'])
    plt.savefig(exe.img_path)

    # 集中力スコアの前日比を計算
    start_date = exe.read_file() # 前回までの日付を取得
    if start_date is None:
        date, time = str(calc.dataset.head(1)['timestamp'].values[0]).split(None,1)
        y, m, d = date.split("-")
        start_date = "{}{}{}".format(y, m, d)
    calc.extract_useable_daily_data(start_date=start_date, daily_path=exe.daily_path)
    print(calc.new_dataset)
    pre_score = 0
    end_idx = calc.new_dataset.tail(1).index[0]
    notification = Notification()
    for index, row in calc.new_dataset.iterrows():
        if index == 0:
            continue
        diff = row['score'] - pre_score
        pre_score = row['score']
        message = exe.make_msg(
            diff=diff, score=row['score'], shot_count=row['shot_count'], date=row['timestamp'])  # メッセージ作成
        if index < end_idx: # 画像なし
            notification.post_simple(msg=message)
            pass
        else:
            notification.post_with_img(msg=message, img_path=exe.img_path)
            pass

    # 通知に利用した最後の日付を保持する
    latest_date = calc.new_dataset.tail(1)['timestamp'].values[0]
    exe.save_file(latest_date)

